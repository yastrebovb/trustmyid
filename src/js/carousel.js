import Siema from 'siema'

class SiemaWithDots extends Siema {
  constructor(props) {
    super(props)

    this.addDots = this.addDots.bind(this)
    this.updateDots = this.updateDots.bind(this)
  }

  addDots() {
    this.dots = document.createElement('div')
    this.dots.classList.add('siema-dots', 'dots')

    for (let i = 0; i < this.innerElements.length; i++) {
      const dot = document.createElement('button')

      dot.classList.add('dots__item')

      dot.addEventListener('click', () => {
        this.goTo(i)
      })

      this.dots.appendChild(dot)
    }

    this.selector.parentNode.insertBefore(this.dots, this.selector.nextSibling)
  }

  updateDots() {
    for (let i = 0; i < this.dots.querySelectorAll('button').length; i++) {
      const addOrRemove = this.currentSlide === i ? 'add' : 'remove'
      this.dots
        .querySelectorAll('button')
        [i].classList[addOrRemove]('dots__item--active')
    }
  }
}

const initCarousels = () => {
  new SiemaWithDots({
    selector: '.siema--iphone',
    duration: 200,
    easing: 'ease-out',
    perPage: 1,
    startIndex: 0,
    draggable: true,
    multipleDrag: true,
    threshold: 20,
    loop: false,
    rtl: false,
    onInit: function() {
      this.addDots()
      this.updateDots()
    },
    onChange: function() {
      this.updateDots()
    }
  })

  new SiemaWithDots({
    selector: '.siema--mac',
    duration: 200,
    easing: 'ease-out',
    perPage: 1,
    startIndex: 0,
    draggable: true,
    multipleDrag: true,
    threshold: 20,
    loop: false,
    rtl: false,
    onInit: function() {
      this.addDots()
      this.updateDots()
    },
    onChange: function() {
      this.updateDots()
    }
  })
}

export default initCarousels
